#!/usr/bin/env bash

kubectl delete pod att-test
docker build -t att-test .
kubectl create -f kb8s_test_pod.yaml
sleep 2
kubectl logs -f att-test