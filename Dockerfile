# docker build -t att-test .

FROM python:2.7

COPY src /opt/tests
COPY pip_requirements.txt /tmp

ENTRYPOINT []

WORKDIR "/opt/tests"

RUN pip install -r /tmp/pip_requirements.txt

CMD bash -c "python test_att_workflow.py"