import sys
import unittest
import argparse
import structlog
import requests
import os
import json
from kafka import KafkaProducer
import time

class TestAttWorkflow(unittest.TestCase):

    def __init__(self, *args, **kwargs):

        self.token = None

        # storing values
        self.url = kwargs['url']
        del kwargs['url']

        self.tosca_url = kwargs['tosca_url']
        del kwargs['tosca_url']

        self.kafka = kwargs['kafka']
        del kwargs['kafka']

        self.username = kwargs['username']
        del kwargs['username']

        self.password = kwargs['password']
        del kwargs['password']

        self.log = structlog.get_logger()

        super(TestAttWorkflow, self).__init__(*args, **kwargs)

    def login(self):
        self.log.info("Logging in", url=self.url, username=self.username, password=self.password)

        url = "http://%s/xosapi/v1/utility/login" % self.url

        res = requests.post(url, json={
            'username': self.username,
            'password': self.password
        })

        if res.status_code != 200:
            raise Exception("Failed to login")

        self.token = res.json()['sessionid']

    def setup_data(self, recipes):
        """
        Execute the TOSCA passed in as parameter
        """
        headers = {
            'xos-username': self.username,
            'xos-password': self.password
        }
        cwd = os.path.dirname(os.path.realpath(__file__))
        for recipe in recipes:
            self.log.info("Loading TOSCA", recipe=recipe)
            filename = os.path.join(cwd, "tosca/%s" % recipe)
            data = open(filename).read()
            res = requests.post("http://%s/run" % self.tosca_url, headers=headers, data=data)


            if res.status_code != 200:
                self.log.error(res.text)
                raise Exception("Failed to send TOSCA recipe %s" % recipe)

    def send_kafka_event(self, event, topic):
        producer = KafkaProducer(bootstrap_servers=self.kafka)
        producer.send(topic, json.dumps(event))
        producer.flush()

    def test_invalid_onu(self):
        self.login()
        self.setup_data(["olt.yaml", "subscriber.yaml"])

        # activate ONU event
        onu_event = {
            'status': 'activated',
            'serial_number': 'invalid',
            'of_dpid': 'of:test_id'
        }
        self.send_kafka_event(onu_event, "onu.events")
        time.sleep(2)  # give time to the event to have it's effects

        # check the SI has been created
        url = "http://%s/xosapi/v1/att-workflow-driver/attworkflowdriverserviceinstances" % self.url
        res = requests.get(url, headers={'x-xossession': self.token})

        si = [x for x in res.json()["items"] if x["serial_number"] == onu_event["serial_number"]][0]

        self.assertEqual(si["valid"], "invalid")

    def test_att_workflow(self):
        self.login()
        self.setup_data(["olt.yaml", "subscriber.yaml", "whitelist.yaml"])

        headers = {'x-xossession': self.token}

        # activate ONU event
        onu_event = {
            'status': 'activated',
            'serial_number': 'BRCM1234',
            'of_dpid': 'of:test_id'
        }
        self.send_kafka_event(onu_event, "onu.events")
        self.log.info("ONU event sent")
        time.sleep(5)  # give time to the event to have it's effects

        # check the SI has been created with the correct status
        url = "http://%s/xosapi/v1/att-workflow-driver/attworkflowdriverserviceinstances" % self.url
        res = requests.get(url, headers=headers)
        si = [x for x in res.json()["items"] if x["serial_number"] == onu_event["serial_number"]][0]
        self.assertEqual(si["valid"], "valid")

        # check that subscriber has no chain
        url = "http://%s/xosapi/v1/rcord/rcordsubscribers" % self.url
        res = requests.get(url, headers=headers)
        sub = [x for x in res.json()["items"] if x["onu_device"] == onu_event["serial_number"]][0]
        self.assertEqual(len(sub["subscribed_links_ids"]), 0)

        self.log.info("ONU event handled correctly")

        # send authentication event
        auth_event = {
            'authenticationState': "APPROVED",
            'deviceId': "of:test_id",
            'portNumber': "101"
        }
        self.send_kafka_event(auth_event, "authentication.events")
        self.log.info("AUTH event sent")
        time.sleep(15)  # give time to the event to have it's effects

        # check that subscriber has status=enabled and chain
        url = "http://%s/xosapi/v1/rcord/rcordsubscribers" % self.url
        res = requests.get(url, headers=headers)
        sub = [x for x in res.json()["items"] if x["onu_device"] == onu_event["serial_number"]][0]
        self.assertEqual(sub["status"], "enabled")
        self.assertEqual(len(sub["subscribed_links_ids"]), 1)

        self.log.info("AUTH event handled correctly")
        # TODO test that volt-service insance and crossconnect service instance have the valid values

        # send dhcp event
        dhcp_event = {
            "macAddress" : "00:AA:00:00:00:01",
            "ipAddress" : "192.168.3.5",
            'deviceId': "of:test_id",
            'portNumber': "101"
        }
        self.send_kafka_event(dhcp_event, "dhcp.events")
        self.log.info("DHCP event sent")
        time.sleep(5)

        # check that subscriber has ip and mac address
        url = "http://%s/xosapi/v1/rcord/rcordsubscribers" % self.url
        res = requests.get(url, headers=headers)
        sub = [x for x in res.json()["items"] if x["onu_device"] == onu_event["serial_number"]][0]
        self.assertEqual(sub["mac_address"], dhcp_event["macAddress"])
        self.assertEqual(sub["ip_address"], dhcp_event["ipAddress"])



if __name__ == '__main__':

    parser = argparse.ArgumentParser(description='Test the AT&T workflow')

    parser.add_argument("-u", "--url", help="XOS Api URL",
                        action="store", default="xos-chameleon:9101")

    parser.add_argument("--toscaurl", help="XOS TOSCA URL",
                        action="store", default="xos-tosca:9102")

    parser.add_argument("-U", "--username", help="XOS Username",
                        action="store", default="admin@opencord.org")

    parser.add_argument("-P", "--password", help="XOS Password",
                        action="store", default="letmein")

    parser.add_argument("-K", "--kafka", help="Kafka URL",
                        action="store", default="cord-kafka")

    args = parser.parse_args()


    test_loader = unittest.TestLoader()
    test_names = test_loader.getTestCaseNames(TestAttWorkflow)

    suite = unittest.TestSuite()
    for test_name in test_names:
        suite.addTest(TestAttWorkflow(test_name,
                                      url=args.url,
                                      tosca_url=args.toscaurl,
                                      kafka=args.kafka,
                                      username=args.username,
                                      password=args.password
            )
        )

    result = unittest.TextTestRunner(verbosity=2).run(suite)
    sys.exit(not result.wasSuccessful())